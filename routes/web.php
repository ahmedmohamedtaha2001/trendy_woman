<?php

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'admin.'], function () {

  // Set Language For Admin 
  Route::get('/lang/{lang}', 'AuthController@SetLanguage');

  // guest routes for admin 
  Route::group(['middleware' => ['guest:admin']], function () {
    Route::get('login', 'AuthController@showLoginForm')->name('show.login')->middleware('guest:admin');
    Route::post('login', 'AuthController@login')->name('login');
  });


  
  Route::get('logout', 'AuthController@logout')->name('logout');
  
  Route::group(['middleware' => ['admin']], function () {

    /*------------ start Of profile----------*/
      Route::get('profile', [
        'uses'  => 'HomeController@profile',
        'as'    => 'profile.profile',
        'title' => 'profile',
      ]);

      Route::put('profile-update', [
        'uses'  => 'HomeController@updateProfile',
        'as'    => 'profile.update',
        'title' => 'update_profile',
      ]);
      Route::put('profile-update-password', [
        'uses'  => 'HomeController@updatePassword',
        'as'    => 'profile.update_password',
        'title' => 'update_password',
      ]);
    /*------------ end Of profile----------*/

    /*------------ start Of Dashboard----------*/
      Route::get('dashboard', [
        'uses'      => 'HomeController@dashboard',
        'as'        => 'dashboard',
        'icon'      => '<i class="feather icon-home"></i>',
        'title'     => 'main_page',
        'has_sub_route' => false,
        'type'      => 'parent',
      ]);
    /*------------ end Of dashboard ----------*/

    /*------------ start Of intro site  ----------*/
      Route::get('intro-site', [
        'icon'      => '<i class="feather icon-map"></i>',
        'title'     => 'introductory_site',
        'type'      => 'parent',
        'has_sub_route' => true,
        'child'     => [
          'intro_settings.index', 'introsliders.show', 'introsliders.index', 'introsliders.store', 'introsliders.update', 'introsliders.delete', 'introsliders.deleteAll', 'introsliders.create', 'introsliders.edit',
          'introservices.show', 'introservices.index', 'introservices.create', 'introservices.store', 'introservices.edit', 'introservices.update', 'introservices.delete', 'introservices.deleteAll',
          'introparteners.create', 'introparteners.show', 'introparteners.index', 'introparteners.store', 'introparteners.update', 'introparteners.delete', 'introparteners.deleteAll',
          'introsocials.show', 'introsocials.index', 'introsocials.store', 'introsocials.update', 'introsocials.delete', 'introsocials.deleteAll', 'introsocials.edit', 'introsocials.create',
          'introparteners.edit',
        ],
      ]);

      Route::get('intro-settings', [
        'uses'      => 'IntroSetting@index',
        'as'        => 'intro_settings.index',
        'title'     => 'introductory_site_setting',
        'sub_link'  => true ,
      ]);

      /*------------ start Of introsliders ----------*/
          Route::get('introsliders', [
            'uses'      => 'IntroSliderController@index',
            'as'        => 'introsliders.index',
            'title'     => 'intro_slider.index',
            'sub_link'  => true ,
          ]);

          # socials store
          Route::get('introsliders/create', [
            'uses'  => 'IntroSliderController@create',
            'as'    => 'introsliders.create',
            'title' => 'intro_slider.create_page',
          ]);

          # introsliders store
          Route::post('introsliders/store', [
            'uses'  => 'IntroSliderController@store',
            'as'    => 'introsliders.store',
            'title' => 'intro_slider.create',
          ]);

          # socials update
          Route::get('introsliders/{id}/edit', [
            'uses'  => 'IntroSliderController@edit',
            'as'    => 'introsliders.edit',
            'title' => 'intro_slider.edit_page',
          ]);

          # introsliders update
          Route::put('introsliders/{id}', [
            'uses'  => 'IntroSliderController@update',
            'as'    => 'introsliders.update',
            'title' => 'intro_slider.edit',
          ]);

          # introsliders update
          Route::get('introsliders/{id}/Show', [
            'uses'  => 'IntroSliderController@show',
            'as'    => 'introsliders.show',
            'title' => 'intro_slider.show',
          ]);

          # introsliders delete
          Route::delete('introsliders/{id}', [
            'uses'  => 'IntroSliderController@destroy',
            'as'    => 'introsliders.delete',
            'title' => 'intro_slider.delete',
          ]);

          #delete all introsliders
          Route::post('delete-all-introsliders', [
            'uses'  => 'IntroSliderController@destroyAll',
            'as'    => 'introsliders.deleteAll',
            'title' => 'intro_slider.delete_all',
          ]);
      /*------------ end Of introsliders ----------*/

      /*------------ start Of introservices ----------*/
        Route::get('introservices', [
          'uses'  => 'IntroServiceController@index',
          'as'    => 'introservices.index',
          'title' => 'our_services.index',
          'sub_link'  => true ,
        ]);
        # introservices update
        Route::get('introservices/{id}/Show', [
          'uses'  => 'IntroServiceController@show',
          'as'    => 'introservices.show',
          'title' => 'our_services.show',
        ]);
        # socials store
        Route::get('introservices/create', [
          'uses'  => 'IntroServiceController@create',
          'as'    => 'introservices.create',
          'title' => 'our_services.create_page',
        ]);
        # introservices store
        Route::post('introservices/store', [
          'uses'  => 'IntroServiceController@store',
          'as'    => 'introservices.store',
          'title' => 'our_services.create',
        ]);

        # socials update
        Route::get('introservices/{id}/edit', [
          'uses'  => 'IntroServiceController@edit',
          'as'    => 'introservices.edit',
          'title' => 'our_services.edit_page',
        ]);

        # introservices update
        Route::put('introservices/{id}', [
          'uses'  => 'IntroServiceController@update',
          'as'    => 'introservices.update',
          'title' => 'our_services.edit',
        ]);

        # introservices delete
        Route::delete('introservices/{id}', [
          'uses'  => 'IntroServiceController@destroy',
          'as'    => 'introservices.delete',
          'title' => 'our_services.delete',
        ]);

        #delete all introservices
        Route::post('delete-all-introservices', [
          'uses'  => 'IntroServiceController@destroyAll',
          'as'    => 'introservices.deleteAll',
          'title' => 'our_services.delete_all',
        ]);
      /*------------ end Of introservices ----------*/

      /*------------ start Of introparteners ----------*/
        Route::get('introparteners', [
          'uses'  => 'IntroPartenerController@index',
          'as'    => 'introparteners.index',
          'title' => 'success_Partners.index',
          'sub_link'  => true ,
        ]);

        # introparteners update
        Route::get('introparteners/{id}/Show', [
          'uses'  => 'IntroPartenerController@show',
          'as'    => 'introparteners.show',
          'title' => 'success_Partners.show',
        ]);

        # socials store
        Route::get('introparteners/create', [
          'uses'  => 'IntroPartenerController@create',
          'as'    => 'introparteners.create',
          'title' => 'success_Partners.create_page',
        ]);

        # introparteners store
        Route::post('introparteners/store', [
          'uses'  => 'IntroPartenerController@store',
          'as'    => 'introparteners.store',
          'title' => 'success_Partners.create',
        ]);

        # introparteners update
        Route::get('introparteners/{id}/edit', [
          'uses'  => 'IntroPartenerController@edit',
          'as'    => 'introparteners.edit',
          'title' => 'success_Partners.edit_page',
        ]);

        # introparteners update
        Route::put('introparteners/{id}', [
          'uses'  => 'IntroPartenerController@update',
          'as'    => 'introparteners.update',
          'title' => 'success_Partners.edit',
        ]);

        # introparteners delete
        Route::delete('introparteners/{id}', [
          'uses'  => 'IntroPartenerController@destroy',
          'as'    => 'introparteners.delete',
          'title' => 'success_Partners.delete',
        ]);

        #delete all introparteners
        Route::post('delete-all-introparteners', [
          'uses'  => 'IntroPartenerController@destroyAll',
          'as'    => 'introparteners.deleteAll',
          'title' => 'success_Partners.delete_all',
        ]);
      /*------------ end Of introparteners ----------*/

      /*------------ start Of introsocials ----------*/
        Route::get('introsocials', [
          'uses'  => 'IntroSocialController@index',
          'as'    => 'introsocials.index',
          'title' => 'socials.index',
          'sub_link'  => true ,
        ]);

        # introsocials update
        Route::get('introsocials/{id}/Show', [
          'uses'  => 'IntroSocialController@show',
          'as'    => 'introsocials.show',
          'title' => 'socials.show',
        ]);
        # introsocials store
        Route::get('introsocials/create', [
          'uses'  => 'IntroSocialController@create',
          'as'    => 'introsocials.create',
          'title' => 'socials.create_page',
        ]);

        # introsocials store
        Route::post('introsocials/store', [
          'uses'  => 'IntroSocialController@store',
          'as'    => 'introsocials.store',
          'title' => 'socials.create',
        ]);
        # introsocials update
        Route::get('introsocials/{id}/edit', [
          'uses'  => 'IntroSocialController@edit',
          'as'    => 'introsocials.edit',
          'title' => 'socials.edit_page',
        ]);

        # introsocials update
        Route::put('introsocials/{id}', [
          'uses'  => 'IntroSocialController@update',
          'as'    => 'introsocials.update',
          'title' => 'socials.edit',
        ]);

        # introsocials delete
        Route::delete('introsocials/{id}', [
          'uses'  => 'IntroSocialController@destroy',
          'as'    => 'introsocials.delete',
          'title' => 'socials.delete',
        ]);

        #delete all introsocials
        Route::post('delete-all-introsocials', [
          'uses'  => 'IntroSocialController@destroyAll',
          'as'    => 'introsocials.deleteAll',
          'title' => 'socials.delete_all',
        ]);
      /*------------ end Of introsocials ----------*/

    /*------------ end Of intro site ----------*/

    /*------------ start Of all users  ----------*/
      Route::get('all-users', [
        'as'        => 'all_users',
        'icon'      => '<i class="feather icon-users"></i>',
        'title'     => 'all_users',
        'type'      => 'parent',
        'has_sub_route' => true,
        'child'     => [
          'clients.index', 'clients.show',  'clients.complaints.show', 'clients.store', 'clients.update', 'clients.delete', 'clients.notify', 'clients.deleteAll', 'clients.create', 'clients.edit', 'clients.block','clients.updateBalance',
          'admins.index', 'admins.store', 'admins.update', 'admins.edit', 'admins.delete', 'admins.deleteAll', 'admins.create', 'admins.edit', 'admins.notifications', 'admins.notifications.delete', 'admins.show',
        ],
      ]);

      /*------------ start Of users Controller ----------*/

        Route::get('clients', [
          'uses'  => 'ClientController@index',
          'as'        => 'clients.index',
          'sub_link'  => true ,
          'title'     => 'users.index',
        ]);

        # clients store
        Route::get( 'clients/create',[
            'uses'  => 'ClientController@create',
            'as'    => 'clients.create', 'clients.edit',
            'title' => 'users.create_page',
          ]
        );

        # clients update
        Route::get('clients/{id}/edit', [
          'uses'  => 'ClientController@edit',
          'as'    => 'clients.edit',
          'title' => 'users.edit_page',
        ]);
        #store
        Route::post('clients/store', [
          'uses'  => 'ClientController@store',
          'as'    => 'clients.store',
          'title' => 'users.create',
        ]);

        #update
        Route::put('clients/{id}', [
          'uses'  => 'ClientController@update',
          'as'    => 'clients.update',
          'title' => 'users.edit',
        ]);

        Route::get('clients/{id}/show', [
          'uses'  => 'ClientController@show',
          'as'    => 'clients.show',
          'title' => 'users.show',
        ]);
        Route::get('clients-complaints/{id}/show', [
          'uses'  => 'ClientController@complaints',
          'as'    => 'clients.complaints.show',
          'title' => 'users.complaints',
        ]);

        #delete
        Route::delete('clients/{id}', [
          'uses'  => 'ClientController@destroy',
          'as'    => 'clients.delete',
          'title' => 'users.delete',
        ]);

        #delete
        Route::post('delete-all-clients', [
          'uses'  => 'ClientController@destroyAll',
          'as'    => 'clients.deleteAll',
          'title' => 'users.delete_all',
        ]);

        #notify
        Route::post(
          'admins/clients/notify',
          [
            'uses'  => 'ClientController@notify',
            'as'    => 'clients.notify',
            'title' => 'users.Send_user_notification',
          ]
        );
        Route::post('admins/clients/update-balance/{id}',
          [
            'uses'  => 'ClientController@updateBalance',
            'as'    => 'clients.updateBalance',
            'title' => 'users.updateBalance',
          ]
        );
        Route::post('admins/clients/block-user',[
            'uses'  => 'ClientController@block',
            'as'    => 'clients.block',
            'title' => 'users.block_user',
          ]
        );
      /*------------ end Of users Controller ----------*/

      /*------------ start Of Admins Controller ----------*/
        Route::get('admins', [
          'uses'  => 'AdminController@index',
          'as'    => 'admins.index',
          'title' => 'admins.index',
          'sub_link'  => true ,
        ]);

        # admins store
        Route::get('show-notifications', [
          'uses'  => 'AdminController@notifications',
          'as'    => 'admins.notifications',
          'title' => 'admins.notification_page',
        ]);

        # admins store
        Route::post('delete-notifications', [
          'uses'  => 'AdminController@deleteNotifications',
          'as'    => 'admins.notifications.delete',
          'title' => 'admins.delete_notification',
        ]);

        # admins store
        Route::get(
          'admins/create',
          [
            'uses'  => 'AdminController@create',
            'as'    => 'admins.create',
            'title' => 'admins.create_page',
          ]
        );

        #store
        Route::post('admins/store', [
          'uses'  => 'AdminController@store',
          'as'    => 'admins.store',
          'title' => 'admins.create',
        ]);

        # admins update
        Route::get('admins/{id}/edit', [
          'uses'  => 'AdminController@edit',
          'as'    => 'admins.edit',
          'title' => 'admins.edit_page',
        ]);
        #update
        Route::put(
          'admins/{id}',
          [
            'uses'  => 'AdminController@update',
            'as'    => 'admins.update',
            'title' => 'admins.edit',
          ]
        );

        Route::get('admins/{id}/show', [
          'uses'  => 'AdminController@show',
          'as'    => 'admins.show',
          'title' => 'admins.show',
        ]);

        #delete
        Route::delete('admins/{id}', [
          'uses'  => 'AdminController@destroy',
          'as'    => 'admins.delete',
          'title' => 'admins.delete',
        ]);

        #delete
        Route::post('delete-all-admins', [
          'uses'  => 'AdminController@destroyAll',
          'as'    => 'admins.deleteAll',
          'title' => 'admins.delete_all',
        ]);

      /*------------ end Of admins Controller ----------*/


    /*------------ end Of all users  ----------*/

    /*------------ start Of public settings ----------*/
        Route::get('public-settings', [
            'as'        => 'public-settings',
            'icon'      => '<i class="feather icon-flag"></i>',
            'title'     => 'public_settings',
            'type'      => 'parent',
            'has_sub_route' => true,
            'child'     => [
                'socials.index', 'socials.show', 'socials.create', 'socials.store', 'socials.show', 'socials.update', 'socials.edit', 'socials.delete', 'socials.deleteAll',
                'roles.index', 'roles.create', 'roles.store', 'roles.edit', 'roles.update', 'roles.delete',
                'settings.index', 'settings.update',
            ],
        ]);

      /*------------ start Of socials ----------*/
        Route::get('socials', [
          'uses'      => 'SocialController@index',
          'as'        => 'socials.index',
          'title'     => 'socials.index',
          'sub_link'  => true ,
        ]);
        # socials update
        Route::get('socials/{id}/Show', [
          'uses'  => 'SocialController@show',
          'as'    => 'socials.show',
          'title' => 'socials.show',
        ]);
        # socials store
        Route::get('socials/create',
          [
            'uses'  => 'SocialController@create',
            'as'    => 'socials.create',
            'title' => 'socials.create_page',
          ]
        );

        # socials store
        Route::post('socials', [
          'uses'  => 'SocialController@store',
          'as'    => 'socials.store',
          'title' => 'socials.create',
        ]);
        # socials update
        Route::get('socials/{id}/edit', [
          'uses'  => 'SocialController@edit',
          'as'    => 'socials.edit',
          'title' => 'socials.edit_page',
        ]);
        # socials update
        Route::put('socials/{id}', [
          'uses'  => 'SocialController@update',
          'as'    => 'socials.update',
          'title' => 'socials.edit',
        ]);

        # socials delete
        Route::delete('socials/{id}', [
          'uses'  => 'SocialController@destroy',
          'as'    => 'socials.delete',
          'title' => 'socials.delete',
        ]);

        #delete all socials
        Route::post('delete-all-socials', [
          'uses'  => 'SocialController@destroyAll',
          'as'    => 'socials.deleteAll',
          'title' => 'socials.delete_all',
        ]);
      /*------------ end Of socials ----------*/

      /*------------ start Of Roles----------*/
        Route::get('roles', [
          'uses'  => 'RoleController@index',
          'as'    => 'roles.index',
          'title' => 'roles.index',
          'sub_link'  => true ,
        ]);

        #add role page
        Route::get('roles/create', [
          'uses'  => 'RoleController@create',
          'as'    => 'roles.create',
          'title' => 'roles.create_page',

        ]);

        #store role
        Route::post('roles/store', [
          'uses'  => 'RoleController@store',
          'as'    => 'roles.store',
          'title' => 'roles.create',
        ]);

        #edit role page
        Route::get('roles/{id}/edit',
          [
            'uses'  => 'RoleController@edit',
            'as'    => 'roles.edit',
            'title' => 'roles.edit_page',
          ]
        );

        #update role
        Route::put('roles/{id}', [
          'uses'  => 'RoleController@update',
          'as'    => 'roles.update',
          'title' => 'roles.edit',
        ]);

        #delete role
        Route::delete('roles/{id}', [
          'uses'  => 'RoleController@destroy',
          'as'    => 'roles.delete',
          'title' => 'roles.delete',
        ]);
      /*------------ end Of Roles----------*/

      /*------------ start of settings ----------*/
      Route::get('settings', [
        'uses'  => 'SettingController@index',
        'as'    => 'settings.index',
        'title' => 'setting.index',
        'sub_link'  => true ,
      ]);

      #update
      Route::put('settings', [
        'uses'  => 'SettingController@update',
        'as'    => 'settings.update',
        'title' => 'setting.edit',
      ]);
      /*------------ end of settings ----------*/

    /*------------ END Of public settings ----------*/

    /*------------ start Of public sections ----------*/
        Route::get('public-sections', [
          'as'        => 'public-sections',
          'icon'      => '<i class="feather icon-flag"></i>',
          'title'     => 'public_sections',
          'type'      => 'parent',
          'has_sub_route' => true,
          'child'     => [
              'all_complaints', 'complaints.delete', 'complaints.deleteAll', 'complaints.show', 'complaint.replay',
          ],
        ]);

      /*------------ end Of intros ----------*/


      /*------------ start Of complaints ----------*/
        Route::get('all-complaints', [
          'as'        => 'all_complaints',
          'uses'      => 'ComplaintController@index',
          'sub_link'  => true ,
          'title'     => 'complaints_and_proposals.index',
        ]);

        # complaint replay
        Route::post('complaints-replay/{id}', [
          'uses'  => 'ComplaintController@replay',
          'as'    => 'complaint.replay',
          'title' => 'complaints_and_proposals.the_replay_of_complaining_or_proposal',
        ]);
        # socials update
        Route::get('complaints/{id}', [
          'uses'  => 'ComplaintController@show',
          'as'    => 'complaints.show',
          'title' => 'complaints_and_proposals.the_resolution_of_complaining_or_proposal',
        ]);


        # complaints delete
        Route::delete('complaints/{id}', [
          'uses'  => 'ComplaintController@destroy',
          'as'    => 'complaints.delete',
          'title' => 'complaints_and_proposals.delete',
        ]);

        #delete all complaints
        Route::post('delete-all-complaints', [
          'uses'  => 'ComplaintController@destroyAll',
          'as'    => 'complaints.deleteAll',
          'title' => 'complaints_and_proposals.delete_all',
        ]);
      /*------------ end Of complaints ----------*/

    /*------------ end Of public sections ----------*/

    /*------------ start Of notifications ----------*/
      Route::get('notifications', [
        'uses'      => 'NotificationController@index',
        'as'        => 'notifications.index',
        'title'     => 'notifications.index',
        'icon'      => '<i class="ficon feather icon-bell"></i>',
        'type'      => 'parent',
        'has_sub_route' => false,
        'child'     => ['notifications.send'],
      ]);

      Route::post('send-notifications', [
        'uses'  => 'NotificationController@sendNotifications',
        'as'    => 'notifications.send',
        'title' => 'notifications.send',
      ]);
    /*------------ end Of notifications ----------*/

    /*------------ start Of fqs ----------*/
      Route::get('fqs', [
        'uses'      => 'FqsController@index',
        'as'        => 'fqs.index',
        'title'     => 'questions_sections.index',
        'icon'      => '<i class="feather icon-alert-circle"></i>',
        'type'      => 'parent',
        'has_sub_route' => false,
        'child'     => ['fqs.show', 'fqs.create', 'fqs.store', 'fqs.edit', 'fqs.update', 'fqs.delete', 'fqs.deleteAll'],
      ]);

      Route::get('fqs/{id}/show', [
        'uses'  => 'FqsController@show',
        'as'    => 'fqs.show',
        'title' => 'questions_sections.show',
      ]);

      # fqs store
      Route::get('fqs/create', [
        'uses'  => 'FqsController@create',
        'as'    => 'fqs.create',
        'title' => 'questions_sections.create_page',
      ]);

      # fqs store
      Route::post('fqs/store', [
        'uses'  => 'FqsController@store',
        'as'    => 'fqs.store',
        'title' => 'questions_sections.create',
      ]);

      # fqs update
      Route::get('fqs/{id}/edit', [
        'uses'  => 'FqsController@edit',
        'as'    => 'fqs.edit',
        'title' => 'questions_sections.edit_page',
      ]);

      # fqs update
      Route::put('fqs/{id}', [
        'uses'  => 'FqsController@update',
        'as'    => 'fqs.update',
        'title' => 'questions_sections.edit',
      ]);

      # fqs delete
      Route::delete('fqs/{id}', [
        'uses'  => 'FqsController@destroy',
        'as'    => 'fqs.delete',
        'title' => 'questions_sections.delete',
      ]);
      #delete all fqs
      Route::post('delete-all-fqs', [
        'uses'  => 'FqsController@destroyAll',
        'as'    => 'fqs.deleteAll',
        'title' => 'questions_sections.delete_all',
      ]);
    /*------------ end Of fqs ----------*/

    /*------------ start Of sms ----------*/
      Route::get('sms', [
        'uses'      => 'SMSController@index',
        'as'        => 'sms.index',
        'title'     => 'message_packages.index',
        'icon'      => '<i class="feather icon-smartphone"></i>',
        'type'      => 'parent',
        'has_sub_route' => false,
        'child'     => ['sms.update', 'sms.change'],
      ]);
      # sms change
      Route::post('sms-change', [
        'uses'  => 'SMSController@change',
        'as'    => 'sms.change',
        'title' => 'message_packages.change',
      ]);
      # sms update
      Route::put('sms/{id}', [
        'uses'  => 'SMSController@update',
        'as'    => 'sms.update',
        'title' => 'message_packages.update',
      ]);

    /*------------ end Of sms ----------*/

    /*------------ start Of pages ----------*/
        Route::get('pages', [
            'uses'      => 'pagesController@index',
            'as'        => 'pages.index',
            'title'     => 'pages.index',
            'icon'      => '<i class="feather icon-image"></i>',
            'type'      => 'parent',
            'sub_route' => false,
            'child'     => ['pages.edit', 'pages.update', 'pages.show']
        ]);


        # pages update
        Route::get('pages/{id}/edit', [
            'uses'  => 'pagesController@edit',
            'as'    => 'pages.edit',
            'title' => 'pages.edit_page'
        ]);

        # pages update
        Route::put('pages/{id}', [
            'uses'  => 'pagesController@update',
            'as'    => 'pages.update',
            'title' => 'pages.edit'
        ]);

        # pages show
        Route::get('pages/{id}/Show', [
            'uses'  => 'pagesController@show',
            'as'    => 'pages.show',
            'title' => 'pages.show',
        ]);
    /*------------ end Of pages ----------*/

    // /*------------ start Of paymentbrands ----------*/
        Route::get('paymentbrands', [
            'uses'      => 'paymentbrandController@index',
            'as'        => 'paymentbrands.index',
            'title'     => 'paymentbrands.index',
            'icon'      => '<i class="feather icon-credit-card"></i>',
            'type'      => 'parent',
            'sub_route' => false,
            'child'     => ['paymentbrands.create', 'paymentbrands.store', 'paymentbrands.edit', 'paymentbrands.update', 'paymentbrands.show', 'paymentbrands.delete', 'paymentbrands.deleteAll']
        ]);

        # paymentbrands store
        Route::get('paymentbrands/create', [
            'uses'  => 'paymentbrandController@create',
            'as'    => 'paymentbrands.create',
            'title' => 'paymentbrands.create_page'
        ]);

        # paymentbrands store
        Route::post('paymentbrands/store', [
            'uses'  => 'paymentbrandController@store',
            'as'    => 'paymentbrands.store',
            'title' => 'paymentbrands.create'
        ]);

        # paymentbrands update
        Route::get('paymentbrands/{id}/edit', [
            'uses'  => 'paymentbrandController@edit',
            'as'    => 'paymentbrands.edit',
            'title' => 'paymentbrands.edit_page'
        ]);

        # paymentbrands update
        Route::put('paymentbrands/{id}', [
            'uses'  => 'paymentbrandController@update',
            'as'    => 'paymentbrands.update',
            'title' => 'paymentbrands.edit'
        ]);

        # paymentbrands show
        Route::get('paymentbrands/{id}/Show', [
            'uses'  => 'paymentbrandController@show',
            'as'    => 'paymentbrands.show',
            'title' => 'paymentbrands.show',
        ]);

        # paymentbrands delete
        Route::delete('paymentbrands/{id}', [
            'uses'  => 'paymentbrandController@destroy',
            'as'    => 'paymentbrands.delete',
            'title' => 'paymentbrands.delete',
        ]);

        # delete all paymentbrands
        Route::post('delete-all-paymentbrands', [
            'uses'  => 'paymentbrandController@destroyAll',
            'as'    => 'paymentbrands.deleteAll',
            'title' => 'paymentbrands.delete_all',
        ]);

    // /*------------ end Of paymentbrands ----------*/

        /*------------ start Of orders ----------*/
        Route::get('orders', [
          'uses'      => 'OrderController@index',
          'as'        => 'orders.index',
          'title'     => 'orders.index',
          'icon'      => '<i class="feather icon-clipboard"></i>',
          'type'      => 'parent',
          'sub_route' => false,
          'child'     => [ 'orders.reject' , 'orders.updateStatus', 'orders.show', 'orders.delete', 'orders.deleteAll']
      ]);


      Route::put('order/update',[
        'uses'  => 'OrderController@updateStatus',
        'as'    => 'orders.updateStatus',
        'title' => 'orders.updateStatus',
      ]);

      Route::put('order/reject',[
        'uses'  => 'OrderController@reject',
        'as'    => 'orders.reject',
        'title' => 'orders.reject',
      ]);

      # orders show
      Route::get('orders/{id}/Show', [
          'uses'  => 'OrderController@show',
          'as'    => 'orders.show',
          'title' => 'orders.show',
      ]);

      # orders delete
      Route::delete('orders/{id}', [
          'uses'  => 'OrderController@destroy',
          'as'    => 'orders.delete',
          'title' => 'orders.delete',
      ]);

      # delete all orders
      Route::post('delete-all-orders', [
          'uses'  => 'OrderController@destroyAll',
          'as'    => 'orders.deleteAll',
          'title' => 'orders.delete_all',
      ]);

  /*------------ end Of orders ----------*/

    /*------------ start Of sizes ----------*/
        Route::get('sizes', [
            'uses'      => 'sizeController@index',
            'as'        => 'sizes.index',
            'title'     => 'sizes.index',
            'icon'      => '<i class="feather icon-bar-chart"></i>',
            'type'      => 'parent',
            'sub_route' => false,
            'child'     => ['sizes.create', 'sizes.store', 'sizes.edit', 'sizes.update', 'sizes.show', 'sizes.delete', 'sizes.deleteAll']
        ]);

        # sizes store
        Route::get('sizes/create', [
            'uses'  => 'sizeController@create',
            'as'    => 'sizes.create',
            'title' => 'sizes.create_page'
        ]);

        # sizes store
        Route::post('sizes/store', [
            'uses'  => 'sizeController@store',
            'as'    => 'sizes.store',
            'title' => 'sizes.create'
        ]);

        # sizes update
        Route::get('sizes/{id}/edit', [
            'uses'  => 'sizeController@edit',
            'as'    => 'sizes.edit',
            'title' => 'sizes.edit_page'
        ]);

        # sizes update
        Route::put('sizes/{id}', [
            'uses'  => 'sizeController@update',
            'as'    => 'sizes.update',
            'title' => 'sizes.edit'
        ]);

        # sizes show
        Route::get('sizes/{id}/Show', [
            'uses'  => 'sizeController@show',
            'as'    => 'sizes.show',
            'title' => 'sizes.show',
        ]);

        # sizes delete
        Route::delete('sizes/{id}', [
            'uses'  => 'sizeController@destroy',
            'as'    => 'sizes.delete',
            'title' => 'sizes.delete',
        ]);

        # delete all sizes
        Route::post('delete-all-sizes', [
            'uses'  => 'sizeController@destroyAll',
            'as'    => 'sizes.deleteAll',
            'title' => 'sizes.delete_all',
        ]);

    /*------------ end Of sizes ----------*/

    /*------------ start Of types ----------*/
        Route::get('types', [
            'uses'      => 'typeController@index',
            'as'        => 'types.index',
            'title'     => 'types.index',
            'icon'      => '<i class="feather icon-type"></i>',
            'type'      => 'parent',
            'sub_route' => false,
            'child'     => ['types.create', 'types.store', 'types.edit', 'types.update', 'types.show', 'types.delete', 'types.deleteAll']
        ]);

        # types store
        Route::get('types/create', [
            'uses'  => 'typeController@create',
            'as'    => 'types.create',
            'title' => 'types.create_page'
        ]);

        # types store
        Route::post('types/store', [
            'uses'  => 'typeController@store',
            'as'    => 'types.store',
            'title' => 'types.create'
        ]);

        # types update
        Route::get('types/{id}/edit', [
            'uses'  => 'typeController@edit',
            'as'    => 'types.edit',
            'title' => 'types.edit_page'
        ]);

        # types update
        Route::put('types/{id}', [
            'uses'  => 'typeController@update',
            'as'    => 'types.update',
            'title' => 'types.edit'
        ]);

        # types show
        Route::get('types/{id}/Show', [
            'uses'  => 'typeController@show',
            'as'    => 'types.show',
            'title' => 'types.show',
        ]);

        # types delete
        Route::delete('types/{id}', [
            'uses'  => 'typeController@destroy',
            'as'    => 'types.delete',
            'title' => 'types.delete',
        ]);

        # delete all types
        Route::post('delete-all-types', [
            'uses'  => 'typeController@destroyAll',
            'as'    => 'types.deleteAll',
            'title' => 'types.delete_all',
        ]);

    /*------------ end Of types ----------*/

    /*------------ start Of packages ----------*/
        Route::get('packages', [
            'uses'      => 'packageController@index',
            'as'        => 'packages.index',
            'title'     => 'packages.index',
            'icon'      => '<i class="feather icon-package"></i>',
            'type'      => 'parent',
            'sub_route' => false,
            'child'     => ['packages.create', 'packages.store', 'packages.edit', 'packages.update', 'packages.show', 'packages.delete', 'packages.deleteAll']
        ]);

        # packages store
        Route::get('packages/create', [
            'uses'  => 'packageController@create',
            'as'    => 'packages.create',
            'title' => 'packages.create_page'
        ]);

        # packages store
        Route::post('packages/store', [
            'uses'  => 'packageController@store',
            'as'    => 'packages.store',
            'title' => 'packages.create'
        ]);

        # packages update
        Route::get('packages/{id}/edit', [
            'uses'  => 'packageController@edit',
            'as'    => 'packages.edit',
            'title' => 'packages.edit_page'
        ]);

        # packages update
        Route::put('packages/{id}', [
            'uses'  => 'packageController@update',
            'as'    => 'packages.update',
            'title' => 'packages.edit'
        ]);

        # packages show
        Route::get('packages/{id}/Show', [
            'uses'  => 'packageController@show',
            'as'    => 'packages.show',
            'title' => 'packages.show',
        ]);

        # packages delete
        Route::delete('packages/{id}', [
            'uses'  => 'packageController@destroy',
            'as'    => 'packages.delete',
            'title' => 'packages.delete',
        ]);

        # delete all packages
        Route::post('delete-all-packages', [
            'uses'  => 'packageController@destroyAll',
            'as'    => 'packages.deleteAll',
            'title' => 'packages.delete_all',
        ]);

    /*------------ end Of packages ----------*/

    /*------------ start Of works ----------*/
        Route::get('works', [
            'uses'      => 'workController@index',
            'as'        => 'works.index',
            'title'     => 'works.index',
            'icon'      => '<i class="feather icon-briefcase"></i>',
            'type'      => 'parent',
            'sub_route' => false,
            'child'     => ['works.create', 'works.store', 'works.edit', 'works.update', 'works.show', 'works.delete', 'works.deleteAll']
        ]);

        # works store
        Route::get('works/create', [
            'uses'  => 'workController@create',
            'as'    => 'works.create',
            'title' => 'works.create_page'
        ]);

        # works store
        Route::post('works/store', [
            'uses'  => 'workController@store',
            'as'    => 'works.store',
            'title' => 'works.create'
        ]);

        # works update
        Route::get('works/{id}/edit', [
            'uses'  => 'workController@edit',
            'as'    => 'works.edit',
            'title' => 'works.edit_page'
        ]);

        # works update
        Route::put('works/{id}', [
            'uses'  => 'workController@update',
            'as'    => 'works.update',
            'title' => 'works.edit'
        ]);

        # works show
        Route::get('works/{id}/Show', [
            'uses'  => 'workController@show',
            'as'    => 'works.show',
            'title' => 'works.show',
        ]);

        # works delete
        Route::delete('works/{id}', [
            'uses'  => 'workController@destroy',
            'as'    => 'works.delete',
            'title' => 'works.delete',
        ]);

        # delete all works
        Route::post('delete-all-works', [
            'uses'  => 'workController@destroyAll',
            'as'    => 'works.deleteAll',
            'title' => 'works.delete_all',
        ]);

    /*------------ end Of works ----------*/



    #new_routes_here

  });


  // redirect to home if url not found
  Route::fallback(function () {
    return redirect()->route('admin.show.login');
  });

  Route::get('export/{export}', 'ExcelController@master')->name('master-export');
  Route::post('import-items', 'ExcelController@importItems')->name('import-items');
});


