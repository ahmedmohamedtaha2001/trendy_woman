<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ChatController;
use App\Http\Controllers\Api\WalletController;
use App\Http\Controllers\Api\SettingController;
use App\Http\Controllers\Api\ComplaintController;
use App\Http\Controllers\Api\User\AuthController;
use App\Http\Controllers\Api\User\UserController;
use App\Http\Controllers\Api\SettlementController;
use App\Http\Controllers\Api\User\ProfileController;
use App\Http\Controllers\Api\User\NotificationController;
use App\Http\Controllers\Api\CountriesAndCitiesController;
use App\Http\Controllers\Api\User\ForgetPasswordController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\HomeController;


Route::group(['middleware'=>['guest:sanctum']],function(){

    // authentication
        Route::post('sign-up'                                 ,[AuthController::class,       'register']);
        Route::patch('activate'                               ,[AuthController::class,       'activate']);
        Route::get('resend-code'                              ,[AuthController::class,       'resendCode']);
        Route::post('sign-in'                                 ,[AuthController::class,       'login']);
    // authentication

    // forgot password
        Route::post('forget-password-send-code'               ,[ForgetPasswordController::class,       'forgetPasswordSendCode']);
        Route::post('forget-password-check-code'              ,[ForgetPasswordController::class,       'forgetPasswordCheckCode']);
        Route::post('reset-password'                          ,[ForgetPasswordController::class,       'resetPassword']);
    // forgot password
});


Route::group(['middleware'=>['OptionalSanctumMiddleware','api-lang']],function(){
    
    Route::get('about',                                   [SettingController::class,     'about']);
    Route::get('terms',                                   [SettingController::class,     'terms']);
    Route::get('privacy',                                 [SettingController::class,     'privacy']);
    Route::get('intros',                                  [SettingController::class,     'intros']);
    Route::get('fqss',                                    [SettingController::class,     'fqss']);
    Route::get('socials',                                 [SettingController::class,     'socials']);
    Route::get('images',                                  [SettingController::class,     'images']);
    Route::get('categories/{id?}',                        [SettingController::class,     'categories']);
    Route::get('is-production',                           [SettingController::class,     'isProduction']);
    
    Route::get('payment-brands',                          [SettingController::class,      'paymentBrands']);

    Route::get('home',                                    [HomeController::class,         'home']);
    Route::get('packages',                                [HomeController::class,         'packages']);
    Route::get('services',                                [HomeController::class,         'services']);
    Route::get('who-we-are',                              [HomeController::class,         'whoWeAre']);
    Route::get('contact-us',                              [HomeController::class,         'contactUs']);
    Route::get('repeated-questions',                      [HomeController::class,         'repeatedQuestions']);
    Route::get('terms-conditions',                        [HomeController::class,         'termsAndConditions']);
    Route::post('new-complaint',                          [ComplaintController::class,    'StoreComplaint']);
    Route::get('get-price',                               [HomeController::class,         'price']);

});

Route::group(['middleware'=>['auth:sanctum','is-active']],function () {

    // authentication
        Route::delete('sign-out'                            ,[AuthController::class,       'logout']);
        Route::post('delete-account'                        ,[AuthController::class,       'deleteAccount']);
    // authentication
        
    // profile
        Route::get('profile'                                  ,[ProfileController::class,       'getProfile']);
        Route::put('update-profile'                           ,[ProfileController::class,       'updateProfile']);
        Route::patch('update-password'                        ,[ProfileController::class,       'updatePassword']);
    // profile
        
    // update phone
        // Route::post('check-password'                          ,[ProfileController::class,       'checkPassword']);
        Route::post('change-phone-send-code'                  ,[ProfileController::class,       'changePhoneSendCode']);
        Route::post('change-phone-resend-code'                ,[ProfileController::class,       'changePhoneReSendCode']);
        Route::post('change-phone-check-code'                 ,[ProfileController::class,       'changePhoneCheckCode']);
    // update phone
    
    // user
        Route::patch('change-lang'                            ,[UserController::class,       'changeLang']);
    // user
    
    // notifications 
        Route::patch('switch-notify'                          ,[NotificationController::class,       'switchNotificationStatus']);
        Route::get('notifications'                            ,[NotificationController::class,       'getNotifications']);
        Route::get('count-notifications'                      ,[NotificationController::class,       'countUnreadNotifications']);
        Route::delete('delete-notification/{notification_id}' ,[NotificationController::class,       'deleteNotification']);
        Route::delete('delete-notifications'                  ,[NotificationController::class,       'deleteNotifications']);
    // notifications 

    // order
        Route::get('orders/{status}'                   ,[OrderController::class , 'index']);
        Route::get('order/{id}'                        ,[OrderController::class , 'show']);
        Route::post('order'                            ,[OrderController::class , 'store']);
        Route::patch('order-pay'                       ,[OrderController::class , 'pay']);
        Route::patch('order-cancel'                    ,[OrderController::class , 'cancel']);
        Route::patch('order-rate'                      ,[OrderController::class , 'rate']);
        Route::get('order-sizes'                       ,[OrderController::class , 'sizes']);
        Route::get('order-types'                       ,[OrderController::class , 'types']);
    // order
    // chat
        // Route::get('create-room',                             [ChatController::class,        'createRoom']);
        // Route::post('create-private-room',                    [ChatController::class,        'createPrivateRoom']);
        // Route::get('room-members/{room}',                     [ChatController::class,        'getRoomMembers']);
        // Route::get('join-room/{room}',                        [ChatController::class,        'joinRoom']);
        // Route::get('leave-room/{room}',                       [ChatController::class,        'leaveRoom']);
        // Route::get('get-room-messages/{room}',                [ChatController::class,        'getRoomMessages']);
        // Route::get('get-room-unseen-messages/{room}',         [ChatController::class,        'getRoomUnseenMessages']);
        // Route::get('get-rooms',                               [ChatController::class,        'getMyRooms']);
        // Route::delete('delete-message-copy/{message}',        [ChatController::class,        'deleteMessageCopy']);
        // Route::post('send-message/{room}',                    [ChatController::class,        'sendMessage']);
        // Route::post('upload-room-file/{room}',                [ChatController::class,        'uploadRoomFile']);
    // chat

});




