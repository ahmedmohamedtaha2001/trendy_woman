<?php

namespace App\Enums;

class PaymentBrandStatus Extends Base
{
    public const ACTIVE     = 'active';

    public const DISABLED   = 'disabled';
}

