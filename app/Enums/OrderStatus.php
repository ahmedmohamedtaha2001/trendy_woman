<?php

namespace App\Enums;

/**
 * Class OrderStatus
 *
 * @method static string all()
 * @method static string|null nameFor($value)
 * @method static array toArray()
 * @method static array forApi()
 * @method static string slug(int $value)
 */
class OrderStatus extends Base
{
    public const WAITING_FOR_APPROVAL       = 0;
    public const PAYMENT_PENDING            = 1;
    public const UNDERWAY                   = 2;
    public const ENDED                      = 3;
    public const CANCELED                   = 4;
    public const REJECTED                   = 5;
}
