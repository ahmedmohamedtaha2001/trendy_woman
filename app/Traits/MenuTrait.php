<?php

namespace App\Traits;

trait MenuTrait {
  public function home() {

    $menu = [
      [
        'name'  => __('routes.admins.index'),
        'count' => \App\Models\Admin::count(),
        'icon'  => 'icon-users',
        'url'   => url('admin/admins'),
      ], [
        'name'  => __('routes.users.index'),
        'count' => \App\Models\User::count(),
        'icon'  => 'icon-users',
        'url'   => url('admin/clients'),
      ], [
        'name'  => __('routes.socials.index'),
        'count' => \App\Models\Social::count(),
        'icon'  => 'icon-thumbs-up',
        'url'   => url('admin/socials'),
      ], [
        'name'  => __('routes.complaints_and_proposals.index'),
        'count' => \App\Models\Complaint::count(),
        'icon'  => 'icon-list',
        'url'   => url('admin/all-complaints'),
      ],[
        'name'  => __("routes.common_questions.index"),
        'count' => \App\Models\Fqs::count(),
        'icon'  => 'icon-list',
        'url'   => url('admin/fqs'),
      ],[
        'name'  => __('routes.message_packages.index'),
        'count' => \App\Models\SMS::count(),
        'icon'  => 'icon-list',
        'url'   => url('admin/sms'),
      ], [
        'name'  => __('routes.Validities.index'),
        'count' => \App\Models\Role::count(),
        'icon'  => 'icon-eye',
        'url'   => url('admin/roles'),
      ], [
        'name'  => __('routes.orders.index'),
        'count' => \App\Models\Order::count(),
        'icon'  => 'icon-clipboard',
        'url'   => url('admin/orders'),
      ], [
        'name'  => __('routes.sizes.index'),
        'count' => \App\Models\Size::count(),
        'icon'  => 'icon-bar-chart',
        'url'   => url('admin/sizes'),
      ], [
        'name'  => __('routes.types.index'),
        'count' => \App\Models\Type::count(),
        'icon'  => 'icon-type',
        'url'   => url('admin/types'),
      ], [
        'name'  => __('routes.packages.index'),
        'count' => \App\Models\Package::count(),
        'icon'  => 'icon-package',
        'url'   => url('admin/packages'),
      ], [
        'name'  => __('routes.works.index'),
        'count' => \App\Models\Work::count(),
        'icon'  => 'icon-briefcase',
        'url'   => url('admin/works'),
      ],
    ];

    return $menu;
  }

  public function introSiteCards() {
    $menu = [
      [
        'name'  => __('routes.introsliders.index'),
        'count' => \App\Models\IntroSlider::count(),
        'icon'  => 'icon-users',
        'url'   => url('admin/introsliders'),
      ],
      [
        'name'  => __('routes.Service_Suite.index'),
        'count' => \App\Models\IntroService::count(),
        'icon'  => 'icon-users',
        'url'   => url('admin/introservices'),
      ],
      [
        'name'  => __('routes.common_questions.index'),
        'count' => \App\Models\IntroFqs::count(),
        'icon'  => 'icon-users',
        'url'   => url('admin/introfqs'),
      ],
      [
        'name'  => __('routes.Success_Partners.index'),
        'count' => \App\Models\IntroPartener::count(),
        'icon'  => 'icon-users',
        'url'   => url('admin/introparteners'),
      ],
      [
        'name'  => __('routes.Customer_messages.index'),
        'count' => \App\Models\IntroMessages::count(),
        'icon'  => 'icon-users',
        'url'   => url('admin/intromessages'),
      ],
      [
        'name'  => __('routes.socials.index'),
        'count' => \App\Models\IntroSocial::count(),
        'icon'  => 'icon-users',
        'url'   => url('admin/introsocials'),
      ],
    ];
    return $menu;
  }

}