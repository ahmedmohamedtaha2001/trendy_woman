<?php

namespace App\Traits;
use Illuminate\Support\Carbon;

trait GeneralTrait {

    public function isCodeCorrect($user = null, $code): bool {
        if (!$user || $code != $user->code || $this->isPast($user->code_expire)
          //|| env('RESET_CODE') != $code
        ) {
          return false;
        }
        return true;
    }

    public function isPast($date){
      $now = Carbon::now();
      if($now->gt($date)){
        return true;
      }
      return false;
    }
}
