<?php

namespace App\Models;

use App\Enums\OrderStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends BaseModel
{   
    use HasFactory;
    protected $fillable = [
        'name',
        'phone',
        'age',
        'gender',
        'occasion',
        'occasion_date',
        'budget',
        'order_num',
        'status',
        'cancel',
        'rating',
        'type_id',
        'size_id',
        'package_id',
        'user_id',
    ];

    public function scopeStatus($query,$condition){
        switch ($condition) {
            case 1:
                return $query->where('status', '0');
               break;
            case 2:
                return $query->whereIn('status', ['1', '2']);
               break;
            case 3:
                return $query->whereIn('status', ['3', '4', '5']);
               break;
            default:
                return $query;
               break;
        }
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function package(){
        return $this->belongsTo(Package::class);
    }


    public function images(){
        return $this->hasMany(OrderImage::class);
    }

    public function getTypeIdAttribute($value)
    {
        return Type::find($value)['name'];
    }

    public function getSizeIdAttribute($value)
    {
        return Size::find($value)['type'];
    }

    // public function getPackageidAttribute($value)
    // {
    //     $package = Package::find($value);
    //     if($package){
    //         return $package['title'];
    //     }
    //     return 'No Package';
    // }

    
    // public function getStatusAttribute($value)
    // {
    //     return ConsultationStatus::nameFor($value);
    // }
}
