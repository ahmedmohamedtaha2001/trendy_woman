<?php

namespace App\Models;

class User extends AuthBaseModel
{
    const IMAGEPATH = 'users';

    protected $casts = [
        'is_notify'   => 'boolean',
        'is_blocked'  => 'boolean',
        'active'      => 'boolean',
    ];

    protected $fillable = [
        'name',
        'country_code',
        'phone',
        'email',
        'password',
        'image',
        'active',
        'is_blocked',
        'is_notify',
        'code',
        'code_expire',
    ];
    
    public function orders(){
        return $this->hasMany(Order::class);
    }
}
