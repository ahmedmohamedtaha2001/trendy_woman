<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;

class Work extends BaseModel
{
    use HasTranslations; 

    const IMAGEPATH = 'works' ; 
    protected $fillable = ['title','description' ,'image'];
    public $translatable = ['title','description'];
    
}
