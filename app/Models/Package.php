<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;

class Package extends BaseModel
{
    use HasTranslations; 

    // const IMAGEPATH = 'packages' ; 
    protected $fillable = ['title','description' ,'price'];
    public $translatable = ['title','description','price'];
    
}
