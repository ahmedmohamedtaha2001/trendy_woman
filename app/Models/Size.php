<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;

class Size extends BaseModel
{
    use HasTranslations; 

    // const IMAGEPATH = 'sizes' ; 
    protected $fillable = ['type'];
    public $translatable = ['type'];
    
}
