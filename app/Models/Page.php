<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Page extends BaseModel
{
    use HasTranslations; 
    protected $fillable = ['title' , 'slug' , 'content'];
    public $translatable = ['title' , 'content'];

    public function getContentAttribute($value) {
        return preg_replace("!\r?\n!", "", $value);
      }
}
