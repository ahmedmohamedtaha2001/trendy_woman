<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;

class Contact extends BaseModel
{
    const IMAGEPATH = 'contacts' ; 
    protected $fillable = ['image','method'];   
}
