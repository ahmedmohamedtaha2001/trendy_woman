<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderImage extends BaseModel
{
    use HasFactory;
    const IMAGEPATH = 'OrderImage'; 
    protected $table = 'order_images';

    protected $fillable = [
        'order_id',
        'image',
    ];
}
