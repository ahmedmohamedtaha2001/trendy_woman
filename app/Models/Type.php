<?php

namespace App\Models;

use Spatie\Translatable\HasTranslations;

class Type extends BaseModel
{
    use HasTranslations; 

    const IMAGEPATH = 'types' ; 
    protected $fillable = ['name','image'];
    public $translatable = ['name'];
    
}
