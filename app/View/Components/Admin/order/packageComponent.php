<?php

namespace App\View\Components\admin\order;

use App\Models\Package;
use Illuminate\View\Component;

class packageComponent extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $package_title;
    public function __construct($package)
    {
        $package = Package::find($package);
        if($package){
            $this->package_title = $package['title'];
        } else {
            $this->package_title = trans("apis.No Package");
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.order.package-component');
    }
}
