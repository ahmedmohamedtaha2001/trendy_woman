<?php

namespace App\View\Components\admin\order;

use Illuminate\View\Component;

class rate extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $rate;
    public function __construct($rate)
    {
        $this->rate = $rate;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.order.rate');
    }
}
