<?php

namespace App\View\Components\admin\order;

use Illuminate\View\Component;

class status extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $status;
    public $id;
    public function __construct($status,$id)
    {
        $this->status = $status;
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.order.status');
    }
}
