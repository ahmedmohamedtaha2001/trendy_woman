<?php

namespace App\Http\Resources\Api;

use App\Models\Size;
use App\Models\Package;
use App\Enums\OrderStatus;
use App\Models\SiteSetting;
use App\Http\Resources\Api\PackageResource;
use App\Http\Resources\Api\OrderImageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"            => $this->id,
            "name"          => $this->name,
            "phone"         => $this->phone,
            "age"           => $this->age,
            "gender"        => $this->gender,
            "occasion"      => $this->occasion,
            "occasion_date" => $this->occasion_date,
            "budget"        => $this->budget,
            "order_num"     => $this->order_num,
            "status"        => OrderStatus::nameFor($this->status),
            "rating"        => $this->rating,
            "cancel"        => $this->cancel,
            "size_id"       => $this->size_id,
            "type_id"       => $this->type_id,
            "user_id"       => $this->user_id,
            "package"       => new PackageResource(Package::find($this->package_id)),
            "price"         => isset($this->package_id) ? null : SiteSetting::where('key','price')->first()['value'],
            "taxes"         => SiteSetting::where('key','taxes')->first()['value'],
            "created_at"    => $this->created_at->diffForHumans(),
            "images"        => OrderImageResource::collection($this->images),
        ];
        
    }
}
