<?php

namespace App\Http\Resources\Api\Settings;

use Illuminate\Http\Resources\Json\JsonResource;

class FqsResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'question' => trans($this->question),
            'answer'   => trans($this->answer),
        ];
    }
}
