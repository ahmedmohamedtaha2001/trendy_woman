<?php

namespace App\Http\Resources\Api;

use App\Models\Package;
use App\Enums\OrderStatus;
use App\Models\SiteSetting;
use App\Http\Resources\Api\PackageResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrdersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // i check if package exist, if it is exists then take the price from it
        // if no package exist, then take price from the price column
        return [
            "id"            => $this->id,
            "order_num"     => $this->order_num,
            "status"        => OrderStatus::nameFor($this->status),
            "package"       => new PackageResource(Package::find($this->package_id)),
            "price"         => isset($this->package_id) ? null : SiteSetting::where('key','price')->first()['value'],
            "taxes"         => SiteSetting::where('key','taxes')->first()['value'],
            "created_at"    => $this->created_at->diffForHumans(),
        ];
    }
}
