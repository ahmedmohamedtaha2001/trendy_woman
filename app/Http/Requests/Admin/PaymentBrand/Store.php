<?php

namespace App\Http\Requests\Admin\PaymentBrand;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Enums\PaymentBrandStatus;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required',
            'type'      => 'required',
            'status'    => 'required|'. Rule::in(PaymentBrandStatus::ACTIVE,PaymentBrandStatus::DISABLED),
            'image'     => 'required|image',
        ];
    }
}
