<?php
namespace App\Http\Requests\Api\User\Auth;
use App\Http\Requests\BaseRequest;

class LoginRequest extends BaseRequest {

  public function rules() {
    return [
      'name_or_phone' => 'required',
      'password'      => 'required',
    ];
  }

  public function prepareForValidation(){
    if(is_numeric($this->name_or_phone)){
      $this->merge([
        'name_or_phone' => fixPhone($this->name_or_phone),
      ]);
    }
  }

}
