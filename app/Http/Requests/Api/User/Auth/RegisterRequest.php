<?php

namespace App\Http\Requests\Api\User\Auth;

use App\Http\Requests\BaseRequest;

class RegisterRequest extends BaseRequest {
  
  public function rules() {
    return [
      'name'            => 'required|max:50|unique:users,name',
      'phone'           => 'required|numeric|digits_between:9,10|unique:users,phone,NULL,id,deleted_at,NULL',
      'email'           => 'required|email|unique:users,email,NULL,id,deleted_at,NULL|max:50',
      'password'        => 'required|min:6|max:100',
      'confirm_password'=> 'same:password',
    ];
  }

  public function prepareForValidation() {
    $this->merge([
      'phone' => fixPhone($this->phone) ,
    ]);
  }

}
