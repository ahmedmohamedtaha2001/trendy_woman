<?php

namespace App\Http\Requests\Api\User\ForgetPassword;

use App\Http\Requests\BaseRequest;

class forgetPasswordSendCodeRequest extends BaseRequest
{
    public function rules() {
        return [
            'phone'        => 'required|numeric|digits_between:9,10|exists:users,phone,deleted_at,NULL',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'phone' => fixPhone($this->phone),
        ]);
    }

}
