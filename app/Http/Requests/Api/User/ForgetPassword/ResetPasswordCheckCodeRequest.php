<?php

namespace App\Http\Requests\Api\User\ForgetPassword;

use App\Models\User;
use App\Models\UserUpdate;
use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordCheckCodeRequest extends FormRequest
{
    public function rules()
    {
        return [
            'phone'             => 'required|exists:users,phone',
            'password'          => 'required|min:6|max:100',
            'confirm_password'  => 'required|same:password',
            'code'              => 'required|max:10',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'phone' => fixPhone($this->phone),
        ]);
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $user = User::where('phone', $this->phone)->first();
            if (!$user) {
                $validator->errors()->add('not_user', trans('auth.failed'));
            }
            $update = $user ? UserUpdate::where(['user_id' => $user->id, 'type' => 'password', 'code' => $this->code])->first() : null;
            if (!$update) {
                $validator->errors()->add('not_user', trans('auth.code_invalid'));
            }
        });
    }
}
