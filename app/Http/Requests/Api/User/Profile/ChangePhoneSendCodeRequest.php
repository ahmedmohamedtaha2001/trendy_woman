<?php

namespace App\Http\Requests\Api\User\Profile;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Hash;

class ChangePhoneSendCodeRequest extends BaseRequest
{
    public function rules() {
        return [
            'password'     => 'required',
            'phone'        => 'required|numeric|digits_between:9,11|unique:users,phone',
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'phone' => fixPhone($this->phone),
        ]);
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            if (!Hash::check($this->password, auth()->user()->password)) {
                $validator->errors()->add('password', trans('auth.incorrect_pass'));
            }
        });
    }

}
