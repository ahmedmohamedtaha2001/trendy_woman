<?php

namespace App\Http\Requests\Api\User\Profile;

use Illuminate\Foundation\Http\FormRequest;

class ChangePhoneCheckCodeRequest extends FormRequest
{
    public function rules() {
        return [
            'code'         => 'required',
        ];
    }
}
