<?php

namespace App\Http\Requests\Api\User\Order;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;

class Show extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        $user = auth('sanctum')->user();
        $user_orders = Order::where('user_id',$user->id)->pluck('id')->toArray();
        if(in_array($this->route('id'),$user_orders)){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
