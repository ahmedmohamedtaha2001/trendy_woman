<?php

namespace App\Http\Requests\Api\User\Order;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              => 'required|string|max:50',
            'phone'             => 'required|max:15',
            'age'               => 'required',
            'gender'            => 'required|in:male,female',
            'size_id'           => 'required|exists:sizes,id',
            'type_id'           => 'required|exists:types,id',
            'occasion'          => 'required|max:50',
            'occasion_date'     => 'required|date',
            'budget'            => 'required',
            'image'             => 'required',
            'package_id'        => 'nullable|exists:packages,id',
        ];
    }

}
