<?php

namespace App\Http\Requests\Api\User\Order;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Order;

class Pay extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth('sanctum')->user();
        $user_orders = Order::where('user_id',$user->id)->pluck('id')->toArray();
        if(in_array($this->order_id,$user_orders)){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'              => 'required|exists:orders,id',
            'paymentbrand_id'       => 'required|exists:payment_brands,id',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $order = Order::find($this->order_id);
            
            if($order->status != '1'){
                $validator->errors()->add('order_id', trans('auth.pay_not_valid'));
            }
        });
    }
}
