<?php

namespace App\Http\Requests\Api\User\Order;

use App\Models\Order;
use Illuminate\Foundation\Http\FormRequest;

class Rate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = auth('sanctum')->user();
        $user_orders = Order::where('user_id',$user->id)->pluck('id')->toArray();
        if(in_array($this->order_id,$user_orders)){
            return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'      => 'required|exists:orders,id',
            'rating'        => 'required|in:1,2,3,4,5',
        ];
    }

    // public function messages(){
    //     return [
    //         'rating.in'     => 'values must be between 0,5',
    //     ];
    // }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $order = Order::find($this->order_id);
            
            if($order->status != '3' || $order->rating != null){
                $validator->errors()->add('order_id', trans('auth.rate_not_valid'));
            }
        });
    }
}
