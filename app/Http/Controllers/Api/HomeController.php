<?php

namespace App\Http\Controllers\Api;

use App\Models\Fqs;
use App\Models\Page;
use App\Models\Work;
use App\Models\Package;
use App\Models\IntroSlider;
use App\Models\SiteSetting;
use App\Models\IntroService;
use Illuminate\Http\Request;
use App\Models\IntroPartener;
use App\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\PageResource;
use App\Http\Resources\Api\WorkResource;
use App\Http\Resources\Api\PackageResource;
use App\Http\Resources\Api\Settings\FqsResource;
use App\Http\Resources\Api\Settings\IntroResource;
use App\Http\Resources\Api\Settings\IntroSliderResource;

class HomeController extends Controller
{
    use ResponseTrait;

    public function home(){

        $logo       = SiteSetting::where('key','logo')->first(['value']);
        $sliders    = IntroSlider::all(['image','title']);
        
        $services   = SiteSetting::where("key",'services_text_'.app()->getLocale())->first(['value']);
        $works      = Work::all(['title','description','image']);
        $whoWeAre   = Page::where('slug','about')->get(['title','content']);
        $partener   = IntroPartener::all(['image']);
        $data = [
            'logo'          => $logo,
            'sliders'       => IntroSliderResource::collection($sliders),
            'services'      => $services,
            'works'         => WorkResource::collection($works),
            'whoWeAre'      => PageResource::collection($whoWeAre),
            'parteners'     => $partener,
            
        ];

        return $this->successData($data);
    }

    public function packages(Request $request){

        $services   = SiteSetting::where("key",'services_text_'.app()->getLocale())->first(['value']);
        $packages = Package::all(['title','description','price']);

        $data = [
            'services'  => $services,
            'packages'  => PackageResource::collection($packages),
        ];
        return $this->successData($data);
    }

    public function services(Request $request){

        $services   = SiteSetting::where("key",'services_text_'.app()->getLocale())->first(['value']);
        $works      = Work::all(['title','description','image']);

        $data = [
            'services'      => $services,
            'works'         => WorkResource::collection($works),
        ];

        return $this->successData($data);
    }

    public function whoWeAre(){
        $whoWeAre   = Page::where('slug','about')->get(['title','content']);
        $data = [
            'whoWeAre'      => PageResource::collection($whoWeAre),
        ];
        return $this->successData($data);
    }

    public function contactUs()
    {
        $whatsapp = SiteSetting::where('key','whatsapp')->first('value');
        $telegram = SiteSetting::where('key','telegram')->first('value');
        $logo     = SiteSetting::where('key','logo')->first('value');

        $data = [
            'whatsapp'  => $whatsapp,
            'telegram'  => $telegram,
            'logo'      => $logo,
        ];
        return $this->successData($data);
    }

    public function repeatedQuestions(){

        $fqs = Fqs::all(['question','answer']);
        $data = [
            'fqs'   => FqsResource::collection($fqs),
        ];
        return $this->successData($data);
    }

    public function termsAndConditions(){
        $termsAndConditions   = Page::where('slug','terms')->get(['title','content']);
        $data = [
            'termsAndConditions'      => PageResource::collection($termsAndConditions),
        ];
        return $this->successData($data);
    }

    public function price(){
        $price = SiteSetting::where('key','price')->first();
        $data = [
            'price' => $price['value'],
        ];
        return $this->successData($data);
    }
}
