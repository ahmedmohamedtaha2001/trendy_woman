<?php

namespace App\Http\Controllers\Api;

use App\Models\Complaint;
use App\Traits\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\StoreComplaintRequest;
use App\Http\Requests\Api\User\Complaint\Store;

class ComplaintController extends Controller
{
    use ResponseTrait;

    public function StoreComplaint(Store $Request)
    {
        Complaint::create($Request->validated() + (['user_id' => auth()->id()]));
        return $this->successMsg(__('apis.complaint_send'));
    }
}
