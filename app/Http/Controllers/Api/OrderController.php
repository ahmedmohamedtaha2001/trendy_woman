<?php

namespace App\Http\Controllers\Api;

use App\Models\Size;
use App\Models\Type;
use App\Models\Order;
use App\Models\Price;
use App\Models\OrderImage;
use App\Models\SiteSetting;
use App\Models\PaymentBrand;
use Illuminate\Http\Request;
use App\Traits\ResponseTrait;
use App\Models\PaymentTransaction;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\Api\SizeResource;
use App\Http\Resources\Api\TypeResource;
use App\Http\Requests\Api\User\Order\Pay;
use App\Http\Resources\Api\OrderResource;
use App\Http\Requests\Api\User\Order\Rate;
use App\Http\Requests\Api\User\Order\Show;
use App\Http\Resources\Api\OrdersResource;
use App\Http\Requests\Api\User\Order\Store;
use App\Http\Requests\Api\User\Order\Cancel;


class OrderController extends Controller
{
    use ResponseTrait;

    public function index($status)
    {
        $user = auth()->user();
        $orders = Order::where('user_id',$user->id)->status($status)->get();
        return $this->successData(OrdersResource::collection($orders));
    }

    public function show(Show $request){
        $user = auth()->user();
        $order = Order::find($request->id);
        return $this->successData(new OrderResource($order));
    }

    public function store(Store $request){
        
        $user = auth('sanctum')->user();
        DB::transaction(function () use($request,$user){
            $order = Order::create([
                'name'              => $request->name,
                'phone'             => $request->phone,
                'age'               => $request->age,
                'gender'            => $request->gender,
                'occasion'          => $request->occasion,
                'occasion_date'     => $request->occasion_date,
                'budget'            => $request->budget,
                'order_num'         => rand(1111,9999),
                'status'            => '0',
                'type_id'           => $request->type_id,
                'size_id'           => $request->size_id,
                'package_id'        => isset($request->package_id) ? $request->package_id : null,
                'user_id'           => $user->id,
            ]);
            
            foreach($request->file('image') as $image){
                OrderImage::create([
                    'order_id'          => $order->id,
                    'image'             => $image,
                ]);
            }
        });
        
        return $this->successMsg(__('apis.consult_sent_succ'));
    }

    public function pay(Pay $request){
        
        $user = auth('sanctum')->user();

        DB::transaction(function () use($request,$user){
            $order = Order::find($request->order_id);
            $order->update(['status'  => '2']);
            $price = SiteSetting::where('key','price')->first()['value'];
            
            $taxes = SiteSetting::where('key','price')->first()['value'];
            $paymentbrand = PaymentBrand::find($request->paymentbrand_id);

            PaymentTransaction::create([
                'payment_getaway'   => $paymentbrand->name,
                'transaction_id'    => 'id_returned_from_transaction',
                'type'              => 1,
                'amount'            => $price + $taxes,
                'currency_code'     => 'RS',
                'status'            => 'completed',
                'getaway_response'  => json_encode(['anything'  => 'anything']),
                'user_id'           => $user->id,
                'order_id'          => $order->id,
                'paymentbrand_id'   => $paymentbrand->id,
            ]);
        });
        
        return $this->successMsg(__('apis.consult_pay_succ'));
    }

    public function cancel(Cancel $request)
    {
        $order = Order::find($request->order_id);
        $order->update([
            'status'  => '4',
            'cancel'  => $request->cancel,
        ]);
             
        return $this->successMsg(__('apis.consult_cancel_succ'));
    }

    public function rate(Rate $request)
    {
        $order = Order::find($request->order_id);
        $order->update([
            'rating'  => $request->rating,
        ]);
             
        return $this->successMsg(__('apis.consult_rate_succ'));
    }

    public function sizes(){
        $sizes = Size::all();
        $data = [
            'sizes' => SizeResource::collection($sizes),
        ];
        return $this->successData($data);
    }

    public function types(){
        $sizes = Type::all();
        $data = [
            'sizes' => TypeResource::collection($sizes),
        ];
        return $this->successData($data);
    }
}
