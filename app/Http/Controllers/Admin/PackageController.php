<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Package\Store;
use App\Http\Requests\Admin\Package\Update;
use App\Models\Package ;
use App\Traits\ReportTrait;


class PackageController extends Controller
{
    public function index($id = null)
    {
        if (request()->ajax()) {
            $packages = Package::search(request()->searchArray)->paginate(30);
            $html = view('admin.packages.table' ,compact('packages'))->render() ;
            return response()->json(['html' => $html]);
        }
        return view('admin.packages.index');
    }

    public function create()
    {
        return view('admin.packages.create');
    }


    public function store(Store $request)
    {
        Package::create($request->validated());
        ReportTrait::addToLog('  اضافه الباقه') ;
        return response()->json(['url' => route('admin.packages.index')]);
    }
    public function edit($id)
    {
        $package = Package::findOrFail($id);
        return view('admin.packages.edit' , ['package' => $package]);
    }

    public function update(Update $request, $id)
    {
        $package = Package::findOrFail($id)->update($request->validated());
        ReportTrait::addToLog('  تعديل الباقه') ;
        return response()->json(['url' => route('admin.packages.index')]);
    }

    public function show($id)
    {
        $package = Package::findOrFail($id);
        return view('admin.packages.show' , ['package' => $package]);
    }
    public function destroy($id)
    {
        $package = Package::findOrFail($id)->delete();
        ReportTrait::addToLog('  حذف الباقه') ;
        return response()->json(['id' =>$id]);
    }

    public function destroyAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Package::WhereIn('id',$ids)->get()->each->delete()) {
            ReportTrait::addToLog('  حذف العديد من الباقات') ;
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
