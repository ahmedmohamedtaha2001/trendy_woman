<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Type\Store;
use App\Http\Requests\Admin\Type\Update;
use App\Models\Type ;
use App\Traits\ReportTrait;


class TypeController extends Controller
{
    public function index($id = null)
    {
        if (request()->ajax()) {
            $types = Type::search(request()->searchArray)->paginate(30);
            $html = view('admin.types.table' ,compact('types'))->render() ;
            return response()->json(['html' => $html]);
        }
        return view('admin.types.index');
    }

    public function create()
    {
        return view('admin.types.create');
    }


    public function store(Store $request)
    {
        Type::create($request->validated());
        ReportTrait::addToLog('  اضافه النوع') ;
        return response()->json(['url' => route('admin.types.index')]);
    }
    public function edit($id)
    {
        $type = Type::findOrFail($id);
        return view('admin.types.edit' , ['type' => $type]);
    }

    public function update(Update $request, $id)
    {
        $type = Type::findOrFail($id)->update($request->validated());
        ReportTrait::addToLog('  تعديل النوع') ;
        return response()->json(['url' => route('admin.types.index')]);
    }

    public function show($id)
    {
        $type = Type::findOrFail($id);
        return view('admin.types.show' , ['type' => $type]);
    }
    public function destroy($id)
    {
        $type = Type::findOrFail($id)->delete();
        ReportTrait::addToLog('  حذف النوع') ;
        return response()->json(['id' =>$id]);
    }

    public function destroyAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Type::WhereIn('id',$ids)->get()->each->delete()) {
            ReportTrait::addToLog('  حذف العديد من الانواع') ;
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
