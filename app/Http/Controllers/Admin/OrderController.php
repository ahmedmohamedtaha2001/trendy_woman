<?php

namespace App\Http\Controllers\Admin;

use App\Models\Size;
use App\Models\User;
use App\Models\Order;
use App\Enums\OrderStatus;
use App\Traits\ReportTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Order\Store;
use App\Http\Requests\Admin\Order\Update;
use App\Notifications\OrderEndedNotification;
use App\Notifications\OrderAcceptedNotification;
use App\Notifications\OrderRejectedNotification;


class OrderController extends Controller
{
    public function index($id = null)
    {
        $sizes = Size::get();
        
        if (request()->ajax()) {
            $orders = Order::search(request()->searchArray)->paginate(30);
            
            $html = view('admin.orders.table' ,compact('orders'))->render() ;
            return response()->json(['html' => $html]);
        }
        return view('admin.Orders.index',compact('sizes'));
    }

    public function create()
    {
        return view('admin.orders.create');
    }

    public function updateStatus(Request $request){
        $order = Order::find($request->id);
        if($order->status == 0){
            $order->update(['status' => '1']);
            // notify that order is accepted
            $this->orderAcceptedNotification($order);
        } else if ($order->status == 2){
            $order->update(['status' => '3']);
            // notify that order is ended
            $this->orderEndedNotification($order);
        } else {
            return response()->json(['message'  => 'Cannot update']);
        }
        return response()->json(['message'  => 'updated successfully']);
    }

    public function reject(Request $request){
        $order = Order::find($request->id);
        if($order->status == 0){
            $order->update(['status' => '5']);
            // notify that order is rejected
            $this->orderRejectedNotification($order);
        } else {
            return response()->json(['message'  => 'Cannot update']);
        }
        return response()->json(['message'  => 'updated successfully']);
    }


    public function store(Store $request)
    {
        Order::create($request->validated());
        ReportTrait::addToLog('  اضافه الطلب') ;
        return response()->json(['url' => route('admin.orders.index')]);
    }
    public function edit($id)
    {
        $Order = Order::findOrFail($id);
        return view('admin.orders.edit' , ['order' => $order]);
    }


    public function show($id)
    {
        $order = Order::findOrFail($id);
        return view('admin.orders.show' , ['Order' => $order]);
    }
    public function destroy($id)
    {
        $order = Order::findOrFail($id)->delete();
        ReportTrait::addToLog('  حذف الطلب') ;
        return response()->json(['id' =>$id]);
    }

    public function destroyAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Order::WhereIn('id',$ids)->get()->each->delete()) {
            ReportTrait::addToLog('  حذف العديد من الطلبات') ;
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }

    private function orderAcceptedNotification($order){
        $order->user->notify(new OrderAcceptedNotification($order));
    }

    private function orderEndedNotification($order){
        $order->user->notify(new OrderEndedNotification($order));
    }

    private function orderRejectedNotification($order){
        $order->user->notify(new OrderRejectedNotification($order));
    }
}
