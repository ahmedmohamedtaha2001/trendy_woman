<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Contact\Store;
use App\Http\Requests\Admin\Contact\Update;
use App\Models\Contact ;
use App\Traits\ReportTrait;


class ContactController extends Controller
{
    public function index($id = null)
    {
        if (request()->ajax()) {
            $contacts = Contact::search(request()->searchArray)->paginate(30);
            $html = view('admin.contacts.table' ,compact('contacts'))->render() ;
            return response()->json(['html' => $html]);
        }
        return view('admin.contacts.index');
    }

    public function create()
    {
        return view('admin.contacts.create');
    }


    public function store(Store $request)
    {
        Contact::create($request->validated());
        // ReportTrait::addToLog('  اضافه الاتصال') ;
        return response()->json(['url' => route('admin.contacts.index')]);
    }
    public function edit($id)
    {
        $contact = Contact::findOrFail($id);
        return view('admin.contacts.edit' , ['contact' => $contact]);
    }

    public function update(Update $request, $id)
    {
        $contact = Contact::findOrFail($id)->update($request->validated());
        // ReportTrait::addToLog('  تعديل الاتصال') ;
        return response()->json(['url' => route('admin.contacts.index')]);
    }

    public function show($id)
    {
        $contact = Contact::findOrFail($id);
        return view('admin.contacts.show' , ['contact' => $contact]);
    }
    public function destroy($id)
    {
        $contact = Contact::findOrFail($id)->delete();
        // ReportTrait::addToLog('  حذف الاتصال') ;
        return response()->json(['id' =>$id]);
    }

    public function destroyAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Contact::WhereIn('id',$ids)->get()->each->delete()) {
            // ReportTrait::addToLog('  حذف العديد من الاتصالات') ;
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
