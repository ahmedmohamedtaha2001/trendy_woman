<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Work\Store;
use App\Http\Requests\Admin\Work\Update;
use App\Models\Work ;
use App\Traits\ReportTrait;


class WorkController extends Controller
{
    public function index($id = null)
    {
        if (request()->ajax()) {
            $works = Work::search(request()->searchArray)->paginate(30);
            $html = view('admin.works.table' ,compact('works'))->render() ;
            return response()->json(['html' => $html]);
        }
        return view('admin.works.index');
    }

    public function create()
    {
        return view('admin.works.create');
    }


    public function store(Store $request)
    {
        Work::create($request->validated());
        ReportTrait::addToLog('  اضافه اعمالنا') ;
        return response()->json(['url' => route('admin.works.index')]);
    }
    public function edit($id)
    {
        $work = Work::findOrFail($id);
        return view('admin.works.edit' , ['work' => $work]);
    }

    public function update(Update $request, $id)
    {
        $work = Work::findOrFail($id)->update($request->validated());
        ReportTrait::addToLog('  تعديل اعمالنا') ;
        return response()->json(['url' => route('admin.works.index')]);
    }

    public function show($id)
    {
        $work = Work::findOrFail($id);
        return view('admin.works.show' , ['work' => $work]);
    }
    public function destroy($id)
    {
        $work = Work::findOrFail($id)->delete();
        ReportTrait::addToLog('  حذف اعمالنا') ;
        return response()->json(['id' =>$id]);
    }

    public function destroyAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Work::WhereIn('id',$ids)->get()->each->delete()) {
            ReportTrait::addToLog('  حذف العديد من اعمالنا') ;
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
