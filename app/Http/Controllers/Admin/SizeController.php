<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Size\Store;
use App\Http\Requests\Admin\Size\Update;
use App\Models\Size ;
use App\Traits\ReportTrait;


class SizeController extends Controller
{
    public function index($id = null)
    {
        if (request()->ajax()) {
            $sizes = Size::search(request()->searchArray)->paginate(30);
            $html = view('admin.sizes.table' ,compact('sizes'))->render() ;
            return response()->json(['html' => $html]);
        }
        return view('admin.sizes.index');
    }

    public function create()
    {
        return view('admin.sizes.create');
    }


    public function store(Store $request)
    {
        Size::create($request->validated());
        ReportTrait::addToLog('  اضافه الحجم') ;
        return response()->json(['url' => route('admin.sizes.index')]);
    }
    public function edit($id)
    {
        $size = Size::findOrFail($id);
        return view('admin.sizes.edit' , ['size' => $size]);
    }

    public function update(Update $request, $id)
    {
        $size = Size::findOrFail($id)->update($request->validated());
        ReportTrait::addToLog('  تعديل الحجم') ;
        return response()->json(['url' => route('admin.sizes.index')]);
    }

    public function show($id)
    {
        $size = Size::findOrFail($id);
        return view('admin.sizes.show' , ['size' => $size]);
    }
    public function destroy($id)
    {
        $size = Size::findOrFail($id)->delete();
        ReportTrait::addToLog('  حذف الحجم') ;
        return response()->json(['id' =>$id]);
    }

    public function destroyAll(Request $request)
    {
        $requestIds = json_decode($request->data);
        
        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Size::WhereIn('id',$ids)->get()->each->delete()) {
            ReportTrait::addToLog('  حذف العديد من الاحجام') ;
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
