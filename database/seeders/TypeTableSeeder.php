<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            [
                'name' => json_encode(['ar' => 'تفاحه' , 'en' => 'Apple'],JSON_UNESCAPED_UNICODE),
                'image' => 'apple.png',
            ],
            [
                'name' => json_encode(['ar' => 'كمثري' , 'en' => 'Pears'],JSON_UNESCAPED_UNICODE),
                'image' => 'pears.png',
            ],
            [
                'name' => json_encode(['ar' => 'مستطيل' , 'en' => 'rectangle'],JSON_UNESCAPED_UNICODE),
                'image' => 'rectangle.png',
            ],
            [
                'name' => json_encode(['ar' => 'مثلث' , 'en' => 'triangle'],JSON_UNESCAPED_UNICODE),
                'image' => 'triangle.png',
            ],
            [
                'name' => json_encode(['ar' => 'ساعه رمليه' , 'en' => 'Sand Glass'],JSON_UNESCAPED_UNICODE),
                'image' => 'sandglass.png',
            ],
            [
                'name' => json_encode(['ar' => 'اخري' , 'en' => 'Another'],JSON_UNESCAPED_UNICODE),
                'image' => 'nope.png',
            ],
        ]);
    }
}
