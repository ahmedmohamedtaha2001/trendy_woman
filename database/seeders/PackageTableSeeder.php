<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->insert([
            [
                'title' => json_encode(['ar' => 'بكج العروس','en' => 'Bride`s package'],JSON_UNESCAPED_UNICODE),
                'description'   => json_encode(['ar' => 'وصف بكج العروس','en' => 'Bride`s package description'],JSON_UNESCAPED_UNICODE),
                'price' => json_encode(['ar' => '200' , 'en' => '200'],JSON_UNESCAPED_UNICODE), 
            ],
            [
                'title' => json_encode(['ar' => 'بكج الجامعه','en' => 'College`s package'],JSON_UNESCAPED_UNICODE),
                'description'   => json_encode(['ar' => 'وصف بكج الجامعه','en' => 'College`s package description'],JSON_UNESCAPED_UNICODE),
                'price' => json_encode(['ar' => '100' , 'en' => '100'],JSON_UNESCAPED_UNICODE), 
            ],
            [
                'title' => json_encode(['ar' => 'بكج الحامل','en' => 'Pregnant`s package'],JSON_UNESCAPED_UNICODE),
                'description'   => json_encode(['ar' => 'وصف بكج الحامل','en' => 'Pregnant`s package description'],JSON_UNESCAPED_UNICODE),
                'price' => json_encode(['ar' => '300' , 'en' => '300'],JSON_UNESCAPED_UNICODE), 
            ],
        ]);
    }
}
