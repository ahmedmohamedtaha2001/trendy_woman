<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
            [
                'type' => json_encode(['ar' => 'كبير' , 'en' => 'L'],JSON_UNESCAPED_UNICODE),
            ],
            [
                'type' => json_encode(['ar' => 'كبير جدا' , 'en' => 'XL'],JSON_UNESCAPED_UNICODE),
            ],
            [
                'type' => json_encode(['ar' => 'كبير جدا جدا' , 'en' => 'XXL'],JSON_UNESCAPED_UNICODE),
            ],
        ]);
    }
}
