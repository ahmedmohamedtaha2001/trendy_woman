<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class WorkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('works')->insert([
            'title' => json_encode(['ar' => 'تنسيق عبايات','en' => 'Abaya Design'],JSON_UNESCAPED_UNICODE),
            'description'   => json_encode(['ar' => 'للمناسبات البسيطه','en' => 'for simple occasions'],JSON_UNESCAPED_UNICODE),
            'image' => 'image.png', 
        ]);
    }
}
