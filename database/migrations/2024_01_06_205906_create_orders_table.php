<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('name',50);
            $table->string('phone',15);
            $table->integer('age');
            $table->enum('gender',['male','female']);
            $table->string('occasion',50);
            $table->date('occasion_date');
            $table->string('budget',5);
            $table->integer('order_num')->unique();
            $table->enum('status',[0,1,2,3,4,5])->default(0);
            $table->integer('rating')->nullable();
            $table->text('cancel')->nullable();
            $table->unsignedBigInteger('size_id');
            $table->unsignedBigInteger('type_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('package_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
