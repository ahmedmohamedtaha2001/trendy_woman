<?php

return [

    'TestNotification'   => 'اختبار الاشعارات',
    'title_finish_order' => 'Awamer',
    'body_finish_order'  => 'طلبك تم :order_num',
    'title_admin_notify' => 'اشعار اداري ',
    'title_finish_order' => 'انهاء طلب',
    'body_finish_order'  => 'تم الانتهاء من المنتج رقم :order_num',
    'title_block'           => 'حظر',
    'body_block'            => 'تم حظرك من قبل الادارة',
    'title_Acception'       => 'موافقه',
    'body_Acception'        => 'تم موافقه الاداره علي طلبك',
    'title_Rejection'       => 'رفض',
    'body_Rejection'        => 'لم توافق الاداره علي طلبك',
    'title_Ended'           => 'انتهاء',
    'body_Ended'            => 'لقد تم الانتهاء من طلبك',
];
