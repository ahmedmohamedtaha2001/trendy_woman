<?php

return [
    'TestNotification'   => 'Test Notification Message En',
    'title_finish_order' => 'Awamer',
    'body_finish_order'  => 'Your order has been finished :order_num',
    'title_admin_notify' => 'Administrative notice',
    'title_block' => 'Block',
    'body_block'  => 'Your Account Is Block From Administrative',
    'title_Acception'       => 'Acception',
    'body_Acception'        => 'Management Accepted your order',
    'title_Rejection'       => 'Rejection',
    'body_Rejection'        => 'Management rejected your order',
    'title_Ended'           => 'Ended',
    'body_Ended'            => 'Management Ended your order',
];
