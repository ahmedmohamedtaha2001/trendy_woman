<?php

return [

  /*
  |--------------------------------------------------------------------------
  | Authentication Language Lines
  |--------------------------------------------------------------------------
  |
  | The following language lines are used during authentication for various
  | messages that we need to display to the user. You are free to modify
  | these language lines according to your application's requirements.
  |
   */

  'failed'                  => 'These credentials do not match our records.',
  'throttle'                => 'Too many login attempts. Please try again in :seconds seconds.',
  'sms'                     => 'Activation code',
  'registered'              => 'Profile created successfully',
  'activated'               => 'account activated successfully',
  'code_expired'            => 'Code expired',
  'code_invalid'            => 'Code invalid',
  'code_re_send'            => 'Code resend',
  'invalid_token'           => 'jwt token invalid',
  'expired_token'           => 'jwt token expired',
  'not_authorized'          => 'You are not authorized',
  'incorrect_pass_or_phone' => 'Incorrect password',
  'not_active'              => 'Account needs activation',
  'incorrect_pass'          => 'Incorrect password',
  'incorrect_old_pass'      => 'Incorrect Old password',
  'incorrect_name_or_phone'  => 'Check the phone number or name',
  'unauthenticated'         => 'Please login again',
  'blocked'                 => 'blocked',
  'account_deleted'         => 'Account Deleted successfully',
  'account_updated'         => 'The account has been successfully modified',
  'same_password'           => 'The new password should matche the current password',
  'retry_proccess'          => 'Repeat the process again',
  'pay_not_valid'           => 'You cant pay now',
  'cancel_not_valid'        => 'You cant cancel',
  'rate_not_valid'          => 'You cant evaluate now',
];
