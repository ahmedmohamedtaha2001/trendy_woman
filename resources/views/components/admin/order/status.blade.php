<div>
    <!-- No surplus words or unnecessary actions. - Marcus Aurelius -->
    @if ($status == 0)
        <div class="text-center m-1">
            <span class="btn btn-sm round btn-outline-success">{{__('admin.approvement')}}</span>
        </div>
        
        <span class="btn btn-sm round btn-outline-success update_status" data-id="{{$id}}">{{__('admin.approve')}}</span>
        <span class="btn btn-sm round btn-outline-danger reject" data-id="{{$id}}">{{__('admin.reject')}}</span>
    @elseif ($status == 1)
    <div class="text-center">
        <span class="btn btn-sm round btn-outline-secondary">{{__('routes.orders.payment_pending')}}</span>  
    </div>
    @elseif ($status == 2)
    <div class="text-center">
        <span class="btn btn-sm round btn-outline-primary update_status" data-id="{{$id}}">{{__('routes.orders.underway')}}</span>
    </div>
    @elseif ($status == 3)
    <div class="text-center">
        <span class="btn btn-sm round btn-outline-info">{{__('routes.orders.ended')}}</span>  
    </div>
    @elseif ($status == 4)
    <div class="text-center">
        <span class="btn btn-sm round btn-outline-danger">{{__('routes.orders.cancel')}}</span>  
    </div>
    @elseif ($status == 5)
    <div class="text-center">
        <span class="btn btn-sm round btn-outline-danger">{{__('routes.orders.rejected')}}</span>  
    </div>
    @endif

</div>