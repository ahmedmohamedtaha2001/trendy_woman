<div>
    <!-- Smile, breathe, and go slowly. - Thich Nhat Hanh -->
    
    @if (isset($rate))
        @for ($i =0; $i < 5;$i++)
            @if ($i < $rate)
                <i class="feather icon-star" style="color:yellow"></i>                                                            
            @endif
            @if ($i >= $rate)
            <i class="feather icon-star"></i>                                                                        
            @endif
        @endfor
    @else
        <div class="alert alert-warning text-center">
            {{ __("admin.no_rating_yet") }}
        </div>
    @endif
    
</div>