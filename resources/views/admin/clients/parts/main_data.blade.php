
<div class="card store">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="form-group">
                    <label for="first-name-column">{{ __('admin.name') }}</label>
                    <div class="controls">
                        <input type="text" name="name" value="{{ $row->name }}"
                            class="form-control"
                            placeholder="{{ __('admin.write_the_name') }}" required
                            data-validation-required-message="{{ __('admin.this_field_is_required') }}">
                    </div>
                </div>
            </div>

            <div class="col-md-6 col-12">
                <div class="form-group">
                    <label for="first-name-column">{{ __('admin.phone_number') }}</label>
                    <div class="controls">
                        <input type="number" name="phone" value="{{ $row->phone }}"
                            class="form-control"
                            placeholder="{{ __('admin.enter_phone_number') }}" required
                            data-validation-required-message="{{ __('admin.this_field_is_required') }}">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="form-group">
                    <label for="first-name-column">{{ __('admin.email') }}</label>
                    <div class="controls">
                        <input type="email" name="email" value="{{ $row->email }}"
                            class="form-control"
                            placeholder="{{ __('admin.enter_the_email') }}" required
                            data-validation-required-message="{{ __('admin.this_field_is_required') }}">
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-6">
                <div class="form-group">
                    <label for="first-name-column">{{ __('admin.status') }}</label>
                    <div class="controls">
                        <select name="is_blocked" class="select2 form-control" required
                            data-validation-required-message="{{ __('admin.this_field_is_required') }}">
                            <option value>{{ __('admin.Select_the_blocking_status') }}
                            </option>
                            <option {{ $row->is_blocked == 1 ? 'selected' : '' }}
                                value="1">{{ __('admin.Prohibited') }}</option>
                            <option {{ $row->is_blocked == 0 ? 'selected' : '' }}
                                value="0">{{ __('admin.Unspoken') }}</option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>