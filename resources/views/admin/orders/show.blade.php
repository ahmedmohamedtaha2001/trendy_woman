@extends('admin.layout.master')

@section('content')
<!-- // Basic multiple Column Form section start -->
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('admin.view') . ' ' . __('routes.orders.order')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form  class="show form-horizontal" >
                            <div class="form-body">
                                <div class="row">
                                    @forelse ($Order->images as $image)
                                    <div class="col-3">
                                        <div class="imgMontg col-12 text-center">
                                            <div class="dropBox">
                                                <div class="textCenter">
                                                    <div class="imagesUploadBlock">
                                                        <div class="uploadedBlock">
                                                            <img src="{{$image->image}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>    
                                    @empty
                                    <div class="alert alert-warning text-center col-12">
                                        {{ __('admin.no_images') }}
                                    </div>
                                    @endforelse
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.name')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->name}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.phone')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->phone}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.age')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->age}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.gender')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->gender}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.occasion')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->occasion}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.occasion_date')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->occasion_date}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.budget')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->budget}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.order_num')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->order_num}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.status')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{App\Enums\orderstatus::nameFor((int)$Order->status)}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.type')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{ $Order->type_id }}" class="form-control" >   
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.size')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->size_id}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="first-name-column">{{__('routes.orders.package')}}</label>
                                            <div class="controls">
                                                <input type="text" name="name" value="{{$Order->package->title}}" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                    @if ($Order->status == 3)
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label for="first-name-column">{{__('routes.orders.rate')}}</label>
                                                <div class="controls">
                                                    <x-admin.order.rate :rate="$Order->rating" />
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($Order->status == 4)
                                        <div class="col-md-12 col-12">
                                            <div class="form-group">
                                                <label for="first-name-column">{{__('routes.orders.cancel_cause')}}</label>
                                                <div class="controls">
                                                    <textarea class="form-control" rows="5">{{ $Order->cancel }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-12 d-flex justify-content-center mt-3">
                                        <x-admin.order.status :status="$Order->status" :id="$Order->id"/>
                                    </div>

                                    <div class="col-12 d-flex justify-content-center mt-3">
                                        <a href="{{ route('admin.orders.index') }}" type="reset" class="btn btn-outline-warning mr-1 mb-1">{{__('admin.back')}}</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')
<script src="{{asset('admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
<script src="{{asset('admin/app-assets/js/scripts/extensions/sweet-alerts.js')}}"></script>
    <script>
        $('.show input').attr('disabled' , true)
        $('.show textarea').attr('disabled' , true)
        $('.show select').attr('disabled' , true)
    </script>
    
    <script>
        $(document).ready(function(){
            $(document).on('click','.update_status',function(e){
                e.preventDefault();
                $.ajax({
                    url: '{{route('admin.orders.updateStatus')}}',
                    method: 'put',
                    data: { id : $(this).data('id')},
                    dataType:'json',
                    beforeSend: function(){
                        // $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').attr('disable',true)
                    },
                    success: function(response){
                        Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: response.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })
                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    },
                });
    
            });

            $(document).on('click','.reject',function(e){
                e.preventDefault();
                $.ajax({
                    url: '{{route('admin.orders.reject')}}',
                    method: 'put',
                    data: { id : $(this).data('id')},
                    dataType:'json',
                    beforeSend: function(){
                        $(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').attr('disable',true)
                    },
                    success: function(response){
                        Swal.fire({
                                    position: 'top-start',
                                    type: 'success',
                                    title: response.message,
                                    showConfirmButton: false,
                                    timer: 1500,
                                    confirmButtonClass: 'btn btn-primary',
                                    buttonsStyling: false,
                                })
                        setTimeout(function(){
                            location.reload();
                        }, 1000);
                    },
                });
    
            });
        });
    </script>
@endsection