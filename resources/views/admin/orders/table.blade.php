<div class="position-relative">
    {{-- table loader  --}}
    <div class="table_loader" >
        {{__('admin.loading')}}
    </div>
    {{-- table loader  --}}
    
    {{-- table content --}}
    <table class="table " id="tab">
        <thead>
            <tr>
                <th>
                    <label class="container-checkbox">
                        <input type="checkbox" value="value1" name="name1" id="checkedAll">
                        <span class="checkmark"></span>
                    </label>
                </th>
                <th>{{__('routes.orders.name')}}</th>
                <th>{{__('routes.orders.phone')}}</th>
                <th>{{__('routes.orders.age')}}</th>
                <th>{{__('routes.orders.gender')}}</th>
                <th>{{__('routes.orders.occasion')}}</th>
                <th>{{__('routes.orders.occasion_date')}}</th>
                <th>{{__('routes.orders.budget')}}</th>
                <th>{{__('routes.orders.order_num')}}</th>
                {{-- <th>{{__('routes.orders.status')}}</th> --}}
                <th>{{__('routes.orders.type')}}</th>
                <th>{{__('routes.orders.size')}}</th>
                <th>{{__('routes.orders.package')}}</th>
                <th>{{ __('routes.orders.change_status') }}</th>
                <th>{{__('admin.control')}}</th>

            </tr>
        </thead>
        <tbody>
            
            @foreach ($orders as $order)
                <tr class="delete_row">
                    <td class="text-center">
                        <label class="container-checkbox">
                        <input type="checkbox" class="checkSingle" id="{{ $order->id }}">
                        <span class="checkmark"></span>
                        </label>
                    </td>
                    <td>{{ $order->name }}</td>
                    <td>{{ $order->phone }}</td>
                    <td>{{ $order->age }}</td>
                    <td>{{ $order->gender }}</td>
                    <td>{{ $order->occasion }}</td>
                    <td>{{ $order->occasion_date }}</td>
                    <td>{{ $order->budget }}</td>
                    <td>{{ $order->order_num }}</td>

                    {{-- <td>{{ $order->status }}</td> --}}
                    <td>{{ $order->type_id }}</td>
                    <td>{{ $order->size_id }}</td>
                    <td>
                        <x-admin.order.package-component :package="$order->package_id" />
                    </td>
                    <td>
                        <x-admin.order.status :status="$order->status" :id="$order->id"/>
                    </td>
                    
                    <td class="product-action"> 
                        <span class="text-primary"><a href="{{ route('admin.orders.show', ['id' => $order->id]) }}"><i class="feather icon-eye"></i></a></span>
                        <span class="delete-row text-danger" data-url="{{ url('admin/orders/' . $order->id) }}"><i class="feather icon-trash"></i></span>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{-- table content --}}
    {{-- no data found div --}}
    @if ($orders->count() == 0)
        <div class="d-flex flex-column w-100 align-center mt-4">
            <img src="{{asset('admin/app-assets/images/pages/404.png')}}" alt="">
            <span class="mt-2" style="font-family: cairo">{{__('admin.there_are_no_matches_matching')}}</span>
        </div>
    @endif
    {{-- no data found div --}}

</div>
{{-- pagination  links div --}}
@if ($orders->count() > 0 && $orders instanceof \Illuminate\Pagination\AbstractPaginator )
    <div class="d-flex justify-content-center mt-3">
        {{$orders->links()}}
    </div>
@endif
{{-- pagination  links div --}}

