@extends('admin.layout.master')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('admin/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('admin/index_page.css')}}">
@endsection

@section('content')

<div class="content-body">

    <div class="mb-1 d-flex justify-content-between m-0">

        <x-admin.buttons 
            extrabuttons="true"
            addbutton="{{ route('admin.sizes.create') }}" 
            deletebutton="{{ route('admin.sizes.deleteAll') }}" 
        >
            <x-slot name="extrabuttonsdiv">
                {{-- <a type="button" data-toggle="modal" data-target="#notify" class="btn bg-gradient-info mr-1 mb-1 waves-effect waves-light notify" data-id="all"><i class="feather icon-bell"></i> {{ __('admin.Send_notification') }}</a> --}}
            </x-slot>
        </x-admin.buttons>

        <x-admin.filter 
            datefilter="true" 
            order="true" 
            :searchArray="[
                'size' => [
                    'input_type' => 'text' , 
                    'input_name' => __('routes.sizes.name') , 
                ] ,

            ]" 
        />

    </div>

    <div class="table_content_append">
        
    </div>
</div>

@endsection

@section('js')

    <script src="{{asset('admin/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    <script src="{{asset('admin/app-assets/js/scripts/extensions/sweet-alerts.js')}}"></script>
    @include('admin.shared.deleteAll')
    @include('admin.shared.deleteOne')
    @include('admin.shared.filter_js' , [ 'index_route' => url('admin/sizes')])
@endsection
