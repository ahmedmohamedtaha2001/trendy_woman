@extends('admin.layout.master')

@section('content')
<!-- // Basic multiple Column Form section start -->
<section id="multiple-column-form">
    <div class="row match-height">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{__('admin.view') . ' ' . __('routes.works.name')}}</h4>
                </div>
                <div class="card-content">
                    <div class="card-body">
                        <form  class="show form-horizontal" >
                            <div class="form-body">
                                <div class="row">
                                    {{-- work edit input here --}}
                                    <div class="col-12">
                                        <div class="imgMontg col-12 text-center">
                                            <div class="dropBox">
                                                <div class="textCenter">
                                                    <div class="imagesUploadBlock">          
                                                        <div class="uploadedBlock">
                                                            <img src="{{$work->image}}">  
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    @foreach (languages() as $lang)
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label for="first-name-column">{{__('routes.works.title_'.$lang)}}</label>
                                                <div class="controls">
                                                    <input type="text" value="{{$work->getTranslations('title')[$lang]}}"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                    @foreach (languages() as $lang)
                                        <div class="col-md-6 col-12">
                                            <div class="form-group">
                                                <label for="first-name-column">{{__('routes.works.description_'.$lang)}}</label>
                                                <div class="controls">
                                                    <input type="text" value="{{$work->getTranslations('description')[$lang]}}"  class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach 

                                    <div class="col-12 d-flex justify-content-center mt-3">
                                        <a href="{{ route('admin.works.index') }}" type="reset" class="btn btn-outline-warning mr-1 mb-1">{{__('admin.back')}}</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')
    <script>
        $('.show input').attr('disabled' , true)
        $('.show textarea').attr('disabled' , true)
        $('.show select').attr('disabled' , true)
    </script>
@endsection